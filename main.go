package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type RoCity struct {
	Rajaongkir Rajaongkir `json:"rajaongkir"`
}
type Status struct {
	Code        int    `json:"code"`
	Description string `json:"description"`
}
type Results struct {
	CityID     string `json:"city_id"`
	ProvinceID string `json:"province_id"`
	Province   string `json:"province"`
	Type       string `json:"type"`
	CityName   string `json:"city_name"`
	PostalCode string `json:"postal_code"`
}
type Rajaongkir struct {
	Query   []interface{} `json:"query"`
	Status  Status        `json:"status"`
	Results []Results     `json:"results"`
}

type RoCost struct {
	Rajaongkir Rajaongkir2 `json:"rajaongkir"`
}
type Query2 struct {
	Origin      string `json:"origin"`
	Destination string `json:"destination"`
	Weight      int    `json:"weight"`
	Courier     string `json:"courier"`
}
type Status2 struct {
	Code        int    `json:"code"`
	Description string `json:"description"`
}
type OriginDetails struct {
	CityID     string `json:"city_id"`
	ProvinceID string `json:"province_id"`
	Province   string `json:"province"`
	Type       string `json:"type"`
	CityName   string `json:"city_name"`
	PostalCode string `json:"postal_code"`
}
type DestinationDetails struct {
	CityID     string `json:"city_id"`
	ProvinceID string `json:"province_id"`
	Province   string `json:"province"`
	Type       string `json:"type"`
	CityName   string `json:"city_name"`
	PostalCode string `json:"postal_code"`
}
type Cost struct {
	Value int    `json:"value"`
	Etd   string `json:"etd"`
	Note  string `json:"note"`
}
type Costs struct {
	Service     string `json:"service"`
	Description string `json:"description"`
	Cost        []Cost `json:"cost"`
}
type Results2 struct {
	Code  string  `json:"code"`
	Name  string  `json:"name"`
	Costs []Costs `json:"costs"`
}
type Rajaongkir2 struct {
	Query              Query2             `json:"query"`
	Status             Status2            `json:"status"`
	OriginDetails      OriginDetails      `json:"origin_details"`
	DestinationDetails DestinationDetails `json:"destination_details"`
	Results            []Results2         `json:"results"`
}

func GetCity(w http.ResponseWriter, r *http.Request) {
	API_KEY := "eb18600cecb89749a19429b213e12d7d"

	url := "https://api.rajaongkir.com/starter/city"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("key", API_KEY)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println("res : ", res)
	fmt.Println("body : ", string(body))

	// Convert response body to struct
	var city RoCity
	json.Unmarshal(body, &city)
	fmt.Printf("API Response as struct %+v\n", city)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(city)
}

func GetCost(w http.ResponseWriter, r *http.Request) {
	API_KEY := "eb18600cecb89749a19429b213e12d7d"

	url := "https://api.rajaongkir.com/starter/cost"

	var query Query2
	b, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(b, &query)
	fmt.Println("bodyR : ", string(b))
	fmt.Println("ori : ", query.Origin)
	fmt.Println("dest : ", query.Destination)

	// payload := strings.NewReader("origin=501&destination=114&weight=1700&courier=jne")
	payload := strings.NewReader("origin=" + query.Origin + "&destination=" + query.Destination + "&weight=1700&courier=jne")

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("key", API_KEY)
	req.Header.Add("content-type", "application/x-www-form-urlencoded")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println("res : ", res)
	fmt.Println("body : ", string(body))

	// Convert response body to struct
	var cost RoCost
	json.Unmarshal(body, &cost)
	fmt.Printf("API Response as struct %+v\n", cost)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(cost)
}

func main() {

	http.HandleFunc("/city", GetCity)
	http.HandleFunc("/cost", GetCost)

	fmt.Println("Starting web server at localhost:1234")
	http.ListenAndServe(":1234", nil)

}
